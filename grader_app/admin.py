from django.contrib import admin
from .models import PythonLab

# Register your models here.
admin.site.register(PythonLab)
