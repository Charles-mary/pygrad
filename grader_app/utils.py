from .labs import lab_1, lab_2, lab_3, lab_4


def create_script(code):
    with open('script.py', 'w') as file:
        file.write(code)


def score_lab(lab_number: int, file: str) -> tuple:
    function_dict = {
        1: lab_1,
        2: lab_2,
        3: lab_3,
        4: lab_4
    }

    return function_dict[lab_number](file)
