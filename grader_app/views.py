import requests
import json

from django.shortcuts import render
from .forms import PythonLabsForm
from grader.settings import env


BASE_URL = env('BASE_URL')

def index(request):
    if request.method == "POST":
        form = PythonLabsForm(request.POST)
        if form.is_valid():

            # endpoint to store result in database
            send_result_url = f"{BASE_URL}/courses/store_lab_result"
            
            # get user id
            resp = requests.get(f"{BASE_URL}/courses/request_user_id/{form.cleaned_data['email']}")
            user_detail = json.loads(resp.content)
            user_id = user_detail.get("userID")

            from .utils import create_script, score_lab

            create_script(request.POST['code'])
            lab_entry = form.save(commit=False)
            score, errors, result = score_lab(form.cleaned_data['lab_number'], 'script')
            lab_entry.score = score

            if not user_id:
                return render(request, 'index.html', {'form': form, 'score': 0, 'errors': ["Invalid email address"]})

            result['lab_id'] = form.cleaned_data['lab_number']
            result['userID'] = user_id

            try:
                requests.post(send_result_url, data=result)
            except:
                print('Error occured')
            

            return render(request, 'index.html', {'form': form, 'score': score, 'errors': errors})
    else:
        form = PythonLabsForm()
    return render(request, 'index.html', {'form': form, 'score': 0, 'errors': []})
