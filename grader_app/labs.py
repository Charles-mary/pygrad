from pathlib import Path
import importlib


BASE_DIR = Path(__file__).resolve().parent.parent


def check_variable(name: str, env: object, value: float, errs: list) -> int:
    variable = getattr(env, name, None)
    if variable:
        if variable == value:
            return 2
        else:
            errs.append(f"Value of {name} variable is incorrect. (correct = {value})")
            return 1
    else:
        errs.append(f"{name} variable not found")
        return 0


def lab_1(file) -> tuple:

    mod = importlib.import_module(file)
    mod = importlib.reload(mod)
    num_of_tasks = 5
    task_dict = {}
    passed = 0
    errors = []


    task_dict['1'] = "passed" if check_variable('principal', mod, 56, errors) == 2 else "failed"
    task_dict['2'] = "passed" if check_variable('rate', mod, 0.05, errors) == 2  else "failed"
    task_dict['3'] = "passed" if check_variable('time', mod, 3, errors) == 2 else "failed"
    task_dict['4'] = "passed" if check_variable('interest', mod, 8.4, errors) == 2 else "failed"
    task_dict['5'] = "passed" if check_variable('compound_interest', mod, 59.16216927877413, errors) == 2  else  "failed"

    for grade in task_dict.values():
        if grade == "passed":
            passed += 1
    
    for i in range(1, 11):
        task_dict[str(i)] = "passed"

    score = passed / num_of_tasks * 100
    Path.unlink(BASE_DIR/(file+".py"))
    del mod
    return score, errors, task_dict


def lab_2(file: str) -> tuple:
    pass


def lab_3(file):
    pass


def lab_4(file):
    pass
