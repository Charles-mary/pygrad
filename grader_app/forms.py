from django.forms import ModelForm
from .models import PythonLab


class PythonLabsForm(ModelForm):

    class Meta:
        model = PythonLab
        fields = ['email', 'lab_number']

