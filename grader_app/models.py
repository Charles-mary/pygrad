from django.db import models

lab_choices = [
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6)
]


class PythonLab(models.Model):

    email = models.EmailField()
    lab_number = models.IntegerField(choices=lab_choices)
    score = models.FloatField()

    def __str__(self):
        return f"email: {self.email} lab: {self.lab_number}"
