pyEditor = CodeMirror.fromTextArea(editor, {
    "lineNumbers": true,
    "mode": "python",
    "lineWrapping": true,
    "matchBrackets": true,
    "indentUnit": 4,
    "theme": "panda-syntax",
    "autoCloseBrackets": true
})
pyEditor.setSize("100%", "500")